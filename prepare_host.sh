#!/bin/bash
export LFS=/mnt/lfs
export LFS_SRC=$LFS/sources
export WGET_LIST=http://linuxfromscratch.org/lfs/downloads/stable/wget-list
export WGET_LIST_MD5=http://linuxfromscratch.org/lfs/downloads/stable/md5sums

echo "Preparing host..."

if [ "`dpkg -l | grep -c texinfo`" == 0 ]; then
	apt-get update && apt-get install texinfo -yqq
fi

if [ "`ls -l /bin/sh | grep -c bash`" == 0 ]; then
	rm /bin/sh
	ln -s /bin/bash /bin/sh
fi

[[ ! -e /dev/sdb1 ]] && echo 'n
p



w
' | fdisk /dev/sdb

if [ ! -d "$LFS" ]; then
	mkdir -pv $LFS
fi
if [ "`mount | grep -c $LFS`" == 0 ]; then
	mkfs.ext4 /dev/sdb1
	mount /dev/sdb1 $LFS
fi

mount | grep $LFS

if [ ! -d "$LFS_SRC" ]; then
	mkdir -v $LFS_SRC
fi

chmod -v a+wt $LFS_SRC
echo "Get LFS stable wget-list"
wget -q $WGET_LIST -O - | while read L; do
	echo "Get $L";
	wget --tries=0 --max-redirect=60 --no-verbose --continue --directory-prefix=$LFS_SRC $L
done
echo "Get and check stable sources md5 checksums"
wget -q $WGET_LIST_MD5 -O $LFS_SRC/md5sums
pushd $LFS_SRC
md5sum -c md5sums | grep -v OK
popd
