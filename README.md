# LFS [Linux from scratch][lfs]
First take on building my own [Linux from scratch][lfs] distribution.

Based on VERSION [8.4][8.4] of [Linux from scratch][lfs] book.

[lfs]: http://linuxfromscratch.org/
[8.4]: http://linuxfromscratch.org/lfs/downloads/stable/LFS-BOOK-8.4-NOCHUNKS.html
[wget-list]: http://linuxfromscratch.org/lfs/downloads/stable/wget-list
